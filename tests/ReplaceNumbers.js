import "@babel/polyfill"; // Babel Object Polyfill to fix Object.assign call
import ReplaceNumbers from "../src/ReplaceNumbers.js";

let howMany;
let numbersMap;
let numbers;
let expectedNumbersList;
let expectedReplacedNumbersList;
let listRange;


beforeEach(() => {
  howMany = 40;
  numbersMap = { 3: 'fizz', 5: 'buzz' };
  numbers = new ReplaceNumbers(howMany, numbersMap);
  // FIXME: need's a not hardcoded approach
  expectedNumbersList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40];
  expectedReplacedNumbersList = [1, 2, "fizz fizz", 4, "buzz buzz", "fizz", 7, 8, "fizz", "buzz", 11, "fizz", "fizz", 14, "lucky", 16, 17, "fizz", 19, "buzz", "fizz", 22, "fizz", "fizz", "buzz buzz", 26, "fizz", 28, 29, "fizz buzz", "fizz", "fizz", "fizz fizz", "fizz", "buzz buzz", "fizz fizz", "fizz", "fizz", "fizz fizz", "buzz"];
  listRange = `${expectedNumbersList[0]} to ${expectedNumbersList.length}`;
});

describe("Upon initialization", () => {
  it(`an array should be automatically created using a range from ${listRange}`, () => {
    expect(expectedNumbersList).toEqual(numbers.list);
  });
  it(`the replaced array should be [1, 2, {...numbers from 3 to ${howMany}}]`, () => {
    expect(expectedReplacedNumbersList).toEqual(numbers.replaced);
  });
  it(`if the end number is 1 or lower and error should be thrown`, () => {
    expect(() => new ReplaceNumbers(1, numbersMap)).toThrow(new Error('End number has to be bigger than 1'));
  });
  it(`if no parameters are given should fallback to defaults`, () => {
    numbers = new ReplaceNumbers();
    expect(expectedNumbersList).toEqual(numbers.list);
    expect(expectedReplacedNumbersList).toEqual(numbers.replaced);
  });
});

describe(`With a numbers list from ${listRange}`, () => {
  it("number 3 should be replaced with 'fizz fizz'", () => {
    expect(numbers.replaced[3-1]).toEqual('fizz fizz');
  });
  it("number 10 should be replaced with 'buzz'", () => {
    expect(numbers.replaced[10 - 1]).toEqual('buzz');
  });
  it("number 15 should be replaced with 'lucky'", () => {
    expect(numbers.replaced[15-1]).toEqual('lucky');
  });
  it("number 30 should be replaced with 'lucky'", () => {
    expect(numbers.replaced[30 - 1]).toEqual('fizz buzz');
  });
});