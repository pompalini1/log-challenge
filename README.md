# How to run the challenge

Assuming that [NodeJS](https://nodejs.org/en/) is installed and this is a requirement:

Clone or [Download](https://gitlab.com/pompalini1/log-challenge/-/archive/master/log-challenge-master.zip) the repository and inside the folder and then in command line run:

    npm install && npm start

This will open the browser and show the challenge result's on http://localhost:9001/

To run the test suite run:

    npm test

This will run the test suite on the command line

If any change is made to the code the browser should auto refresh itself.
