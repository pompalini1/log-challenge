/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Numbers.js":
/*!************************!*\
  !*** ./src/Numbers.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Numbers; });\n/* harmony import */ var _ReplaceNumbers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ReplaceNumbers */ \"./src/ReplaceNumbers.js\");\n\n\nconst howMany = 80;\nconst numbersMap = { 3: 'fizz', 5: 'buzz' };\nconst numbers = new _ReplaceNumbers__WEBPACK_IMPORTED_MODULE_0__[\"default\"](howMany, numbersMap);\n\nclass Numbers {\n  constructor(){\n    return this.html();\n  }\n\n  html(){\n    const element = document.createElement('div');\n    const numbersBeforeTitle = document.createElement('h2');\n    const numbersAfterTitle = document.createElement('h2');\n    const numbersBefore = document.createElement('div');\n    const numbersAfter = document.createElement('div');\n\n    numbersBeforeTitle.innerHTML = 'For the following numbers list:';\n    numbersBefore.className = 'numbers';\n    numbersBefore.innerHTML = numbers.list;\n    numbersAfterTitle.innerHTML = 'This is the result:';\n    numbersAfter.className = 'numbers';\n    numbersAfter.innerHTML = numbers.replaced;\n\n    element.appendChild(numbersBeforeTitle);\n    element.appendChild(numbersBefore);\n    element.appendChild(numbersAfterTitle);\n\n    element.appendChild(numbersAfter);\n    return element;\n  }\n}\n\n//# sourceURL=webpack:///./src/Numbers.js?");

/***/ }),

/***/ "./src/ReplaceNumbers.js":
/*!*******************************!*\
  !*** ./src/ReplaceNumbers.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/**\n  * @class ReplaceNumbers() replaces the numbers to strings in an Array of Numbers\n  *  if any of following Statements are true:\n  *   1. Whenever a number multiple of 3 occurs or contains 3 in its representation, it should be replaced by the word “fizz”;\n  *   2. If it is a multiple of 5, or it contains 5 in its representation, it should be changed to “buzz”;\n  *   3. If it is a multiple of 3 and 5 it should be changed to “fizz buzz”;\n  *   4. If both rule 1 or rule 2 and rule 3 are checked at the same time, it should be changed to “lucky”;\n  *   5. Rules 1 and 2 are cumulative. Example: 3 should be changed to “fizz fizz”.\n  *  @param {Number} endNumber -  how many numbers are to be added to the array for comparison, defaults to 40\n  *  @param {Object} numbersMap - object containing the numbers to be compared and respective strings to replace, defaults to { 3: 'fizz', 5: 'buzz'}\n*/\nclass ReplaceNumbers {\n  constructor(endNumber = 40, { ...numbersMap } = { 3: 'fizz', 5: 'buzz'}) {\n    this.replaced = [];\n    this.list = this.arrayWithRange(1, endNumber);\n    this.replaceMap = numbersMap;\n\n    const numbersToBeReplaced = this.getNumbersToReplace(numbersMap);\n    this.replaceNumbers(numbersToBeReplaced);\n  }\n\n  getNumbersToReplace(Object) {\n    const numbers = [];\n    for (var key in Object) {\n      if (Object.hasOwnProperty(key)) {\n        numbers.push(parseInt(key));\n      }\n    }\n    return numbers;\n  }\n\n  arrayWithRange(start, end) {\n    start = parseInt(start);\n    end = parseInt(end);\n    if (start >= end) {\n      throw new Error('End number has to be bigger than 1');\n    }\n    const ranged = [];\n    for (let i = start; i <= end; i++) {\n      ranged.push(i);\n    }\n    return ranged;\n  }\n\n  replaceNumbers([...numbersToReplace]) {\n    const list = Object.assign([], this.list);\n    let prevNumber = null;\n\n    // We need to check cross check if conditions were already met previously\n    // hence we need to iterate for all numbers to be replaced\n    numbersToReplace.map((number) => {\n      list.map((value, index) => {\n        this.compareNumbers(number, index, list, prevNumber);\n      });\n      // store the current number to compare on next iteration\n      prevNumber = number;\n    });\n\n    this.replaced = list;\n  }\n\n  compareNumbers(number, index, list, prevNumber) {\n    // get the number we want to compare from immutable list\n    const numberValue = this.list[index];\n    const stringToReplace = this.replaceMap[number];\n    // comparissons can be made now\n    const isMultiple = numberValue % number === 0; // Statement 1. is multiple\n    const hasNumber = numberValue.toString().indexOf(number) > -1; // Statement 1. has number in it\n    const isMultipleOfPrevious = isMultiple && numberValue % prevNumber === 0; // Statement 3. is met\n    const bingo = isMultiple && hasNumber && isMultipleOfPrevious; // Statement 4. is met\n    const isMultipleAndHasNumber = isMultiple && hasNumber; // Statement 5. is met\n\n    if (bingo) {\n      return list[index] = 'lucky';\n    }\n\n    if (isMultipleOfPrevious) {\n      return list[index] = `${this.replaceMap[prevNumber]} ${stringToReplace}`;\n    }\n\n    if (isMultiple || hasNumber) {\n      list[index] = stringToReplace;\n    }\n\n    if (isMultipleAndHasNumber) {\n      list[index] = `${list[index]} ${stringToReplace}`;\n    }\n  }\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (ReplaceNumbers);\n\n//# sourceURL=webpack:///./src/ReplaceNumbers.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Numbers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Numbers */ \"./src/Numbers.js\");\n\n\n\nfunction component() {\n  // create our App with all the necessary html DOM elements\n  const element = new _Numbers__WEBPACK_IMPORTED_MODULE_0__[\"default\"]();\n  return element;\n};\n\nconst container = document.getElementById('root');\ncontainer.appendChild(component());\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ })

/******/ });