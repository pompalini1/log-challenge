const path = require('path');
const OpenBrowserPlugin = require('open-browser-webpack-plugin');
const LiveReloadPlugin = require('webpack-livereload-plugin');

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  plugins: [
    new OpenBrowserPlugin({
      url: `http://localhost:${process.env.DEFAULT_PORT}/`,
    }),
    new LiveReloadPlugin({
      appendScriptTag: true
    })
  ],
};