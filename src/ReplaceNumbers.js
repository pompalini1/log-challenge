/**
  * @class ReplaceNumbers() replaces the numbers to strings in an Array of Numbers
  *  if any of following Statements are true:
  *   1. Whenever a number multiple of 3 occurs or contains 3 in its representation, it should be replaced by the word “fizz”;
  *   2. If it is a multiple of 5, or it contains 5 in its representation, it should be changed to “buzz”;
  *   3. If it is a multiple of 3 and 5 it should be changed to “fizz buzz”;
  *   4. If both rule 1 or rule 2 and rule 3 are checked at the same time, it should be changed to “lucky”;
  *   5. Rules 1 and 2 are cumulative. Example: 3 should be changed to “fizz fizz”.
  *  @param {Number} endNumber -  how many numbers are to be added to the array for comparison, defaults to 40
  *  @param {Object} numbersMap - object containing the numbers to be compared and respective strings to replace, defaults to { 3: 'fizz', 5: 'buzz'}
*/
class ReplaceNumbers {
  constructor(endNumber = 40, { ...numbersMap } = { 3: 'fizz', 5: 'buzz'}) {
    this.replaced = [];
    this.list = this.arrayWithRange(1, endNumber);
    this.replaceMap = numbersMap;

    const numbersToBeReplaced = this.getNumbersToReplace(numbersMap);
    this.replaceNumbers(numbersToBeReplaced);
  }

  getNumbersToReplace(Object) {
    const numbers = [];
    for (var key in Object) {
      if (Object.hasOwnProperty(key)) {
        numbers.push(parseInt(key));
      }
    }
    return numbers;
  }

  arrayWithRange(start, end) {
    start = parseInt(start);
    end = parseInt(end);
    if (start >= end) {
      throw new Error('End number has to be bigger than 1');
    }
    const ranged = [];
    for (let i = start; i <= end; i++) {
      ranged.push(i);
    }
    return ranged;
  }

  replaceNumbers([...numbersToReplace]) {
    const list = Object.assign([], this.list);
    let prevNumber = null;

    // We need to check cross check if conditions were already met previously
    // hence we need to iterate for all numbers to be replaced
    numbersToReplace.map((number) => {
      list.map((value, index) => {
        this.compareNumbers(number, index, list, prevNumber);
      });
      // store the current number to compare on next iteration
      prevNumber = number;
    });

    this.replaced = list;
  }

  compareNumbers(number, index, list, prevNumber) {
    // get the number we want to compare from immutable list
    const numberValue = this.list[index];
    const stringToReplace = this.replaceMap[number];
    // comparissons can be made now
    const isMultiple = numberValue % number === 0; // Statement 1. is multiple
    const hasNumber = numberValue.toString().indexOf(number) > -1; // Statement 1. has number in it
    const isMultipleOfPrevious = isMultiple && numberValue % prevNumber === 0; // Statement 3. is met
    const bingo = isMultiple && hasNumber && isMultipleOfPrevious; // Statement 4. is met
    const isMultipleAndHasNumber = isMultiple && hasNumber; // Statement 5. is met

    if (bingo) {
      return list[index] = 'lucky';
    }

    if (isMultipleOfPrevious) {
      return list[index] = `${this.replaceMap[prevNumber]} ${stringToReplace}`;
    }

    if (isMultiple || hasNumber) {
      list[index] = stringToReplace;
    }

    if (isMultipleAndHasNumber) {
      list[index] = `${list[index]} ${stringToReplace}`;
    }
  }
}

export default ReplaceNumbers;