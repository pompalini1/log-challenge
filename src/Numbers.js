import ReplaceNumbers from './ReplaceNumbers';

const howMany = 80;
const numbersMap = { 3: 'fizz', 5: 'buzz' };
const numbers = new ReplaceNumbers(howMany, numbersMap);

export default class Numbers {
  constructor(){
    return this.html();
  }

  html(){
    const element = document.createElement('div');
    const numbersBeforeTitle = document.createElement('h2');
    const numbersAfterTitle = document.createElement('h2');
    const numbersBefore = document.createElement('div');
    const numbersAfter = document.createElement('div');

    numbersBeforeTitle.innerHTML = 'For the following numbers list:';
    numbersBefore.className = 'numbers';
    numbersBefore.innerHTML = numbers.list;
    numbersAfterTitle.innerHTML = 'This is the result:';
    numbersAfter.className = 'numbers';
    numbersAfter.innerHTML = numbers.replaced;

    element.appendChild(numbersBeforeTitle);
    element.appendChild(numbersBefore);
    element.appendChild(numbersAfterTitle);

    element.appendChild(numbersAfter);
    return element;
  }
}