import Numbers from './Numbers';


function component() {
  // create our App with all the necessary html DOM elements
  const element = new Numbers();
  return element;
};

const container = document.getElementById('root');
container.appendChild(component());